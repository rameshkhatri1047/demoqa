import Homepage from "../pageobjects/Homepage";

const objHomepage = new Homepage();
describe('Homepage test', function () {
    beforeEach(function () {
        cy.visit(Cypress.env('devUrl'));
        cy.wait(2000)
    })
    it('Navigate to Homepage',() =>{
        cy.title().should('eq', 'ToolsQA');
    })

    it('Navigate to Element Page', () =>{
        objHomepage.navToElementPage();
        cy.title().should('eq', 'ToolsQA');
    })

})