/*Selectors*/
let elementLink = ':nth-child(1) > :nth-child(1) > .avatar > svg'

class Homepage {
    constructor() { };
    navToElementPage() {
        cy.get(elementLink).click()    }
}

export default Homepage;